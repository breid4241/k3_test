variable "k3_nodes" {
    default = [
       {
           name = "k3-master-01"
           ip_address = "10.10.10.111"
       },
       {
           name = "k3-worker-01"
           ip_address = "10.10.10.112"
       },
       {
           name = "k3-worker-02"
           ip_address = "10.10.10.113"
       }
   ]
}
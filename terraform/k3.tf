module "terraform_pve" {
    source = "git@gitlab.homelab.com:breid01/terraform_pve.git"
    cpu = 1
    memory = 2000
    vm_desc = "k3 nodes"
    vm_configs = var.k3_nodes
}

